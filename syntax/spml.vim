" Vim syntax file
" Language: SPML
" Maintainer: Mario Schäper
" Latest Revision: 01 June 2019

if exists("b:current_syntax")
	finish
endif

" Keywords
syn region Tag start=/<[\/]\?/ms=e+1 end=/[\/]\?>/me=s-1 contains=AttributeGroup fold
syn match XMLTag /<?\zs\_s\{-}xml\ze\_.\{-}?>/
syn match Attribute /[a-zA-Z0-9_-]\+\ze=/ contained
syn match AttributeGroup /[a-zA-Z0-9_-]\+="[^"]*"/ contained contains=String,Attribute

" attributes

" attribute error
syn match AttributeError /[a-zA-Z0-9_-]\+\ze=/ contained
syn match AttributeErrorGroup /[a-zA-Z0-9_-]\+="[^"]*"/ contained contains=String,AttributeError

" absolute attribute
syn match AbsoluteAttribute /absolute\ze=/ contained
syn match AbsoluteAttributeGroup /absolute="[^"]*"/ contained contains=String,AbsoluteAttribute

" action attribute
syn match ActionAttribute /action\ze=/ contained
syn match ActionAttributeGroup /action="[^"]*"/ contained contains=String,ActionAttribute

" align attribute
syn match AlignAttribute /align\ze=/ contained
syn match AlignAttributeGroup /align="[^"]*"/ contained contains=String,AlignAttribute

" alt attribute
syn match AltAttribute /alt\ze=/ contained
syn match AltAttributeGroup /alt="[^"]*"/ contained contains=String,AltAttribute

" anchor attribute
syn match AnchorAttribute /anchor\ze=/ contained
syn match AnchorAttributeGroup /anchor="[^"]*"/ contained contains=String,AnchorAttribute

" api attribute
syn match ApiAttribute /api\ze=/ contained
syn match ApiAttributeGroup /api="[^"]*"/ contained contains=String,ApiAttribute

" arg attribute
syn match ArgAttribute /arg\ze=/ contained
syn match ArgAttributeGroup /arg="[^"]*"/ contained contains=String,ArgAttribute

" arguments attribute
syn match ArgumentsAttribute /arguments\ze=/ contained
syn match ArgumentsAttributeGroup /arguments="[^"]*"/ contained contains=String,ArgumentsAttribute

" attribute attribute
syn match AttributeAttribute /attribute\ze=/ contained
syn match AttributeAttributeGroup /attribute="[^"]*"/ contained contains=String,AttributeAttribute

" attributes attribute
syn match AttributesAttribute /attributes\ze=/ contained
syn match AttributesAttributeGroup /attributes="[^"]*"/ contained contains=String,AttributesAttribute

" background attribute
syn match BackgroundAttribute /background\ze=/ contained
syn match BackgroundAttributeGroup /background="[^"]*"/ contained contains=String,BackgroundAttribute

" bgcolor attribute
syn match BgcolorAttribute /bgcolor\ze=/ contained
syn match BgcolorAttributeGroup /bgcolor="[^"]*"/ contained contains=String,BgcolorAttribute

" bgcolor2 attribute
syn match Bgcolor2Attribute /bgcolor2\ze=/ contained
syn match Bgcolor2AttributeGroup /bgcolor2="[^"]*"/ contained contains=String,Bgcolor2Attribute

" captcharequired attribute
syn match CaptcharequiredAttribute /captcharequired\ze=/ contained
syn match CaptcharequiredAttributeGroup /captcharequired="[^"]*"/ contained contains=String,CaptcharequiredAttribute

" checked attribute
syn match CheckedAttribute /checked\ze=/ contained
syn match CheckedAttributeGroup /checked="[^"]*"/ contained contains=String,CheckedAttribute

" classname attribute
syn match ClassnameAttribute /classname\ze=/ contained
syn match ClassnameAttributeGroup /classname="[^"]*"/ contained contains=String,ClassnameAttribute

" client attribute
syn match ClientAttribute /client\ze=/ contained
syn match ClientAttributeGroup /client="[^"]*"/ contained contains=String,ClientAttribute

" code attribute
syn match CodeAttribute /code\ze=/ contained
syn match CodeAttributeGroup /code="[^"]*"/ contained contains=String,CodeAttribute

" collection attribute
syn match CollectionAttribute /collection\ze=/ contained
syn match CollectionAttributeGroup /collection="[^"]*"/ contained contains=String,CollectionAttribute

" color attribute
syn match ColorAttribute /color\ze=/ contained
syn match ColorAttributeGroup /color="[^"]*"/ contained contains=String,ColorAttribute

" color2 attribute
syn match Color2Attribute /color2\ze=/ contained
syn match Color2AttributeGroup /color2="[^"]*"/ contained contains=String,Color2Attribute

" cols attribute
syn match ColsAttribute /cols\ze=/ contained
syn match ColsAttributeGroup /cols="[^"]*"/ contained contains=String,ColsAttribute

" command attribute
syn match CommandAttribute /command\ze=/ contained
syn match CommandAttributeGroup /command="[^"]*"/ contained contains=String,CommandAttribute

" condition attribute
syn match ConditionAttribute /condition\ze=/ contained
syn match ConditionAttributeGroup /condition="[^"]*"/ contained contains=String,ConditionAttribute

" config attribute
syn match ConfigAttribute /config\ze=/ contained
syn match ConfigAttributeGroup /config="[^"]*"/ contained contains=String,ConfigAttribute

" configextension attribute
syn match ConfigextensionAttribute /configextension\ze=/ contained
syn match ConfigextensionAttributeGroup /configextension="[^"]*"/ contained contains=String,ConfigextensionAttribute

" configvalues attribute
syn match ConfigvaluesAttribute /configvalues\ze=/ contained
syn match ConfigvaluesAttributeGroup /configvalues="[^"]*"/ contained contains=String,ConfigvaluesAttribute

" connect attribute
syn match ConnectAttribute /connect\ze=/ contained
syn match ConnectAttributeGroup /connect="[^"]*"/ contained contains=String,ConnectAttribute

" contentType attribute
syn match ContentTypeAttribute /contentType\ze=/ contained
syn match ContentTypeAttributeGroup /contentType="[^"]*"/ contained contains=String,ContentTypeAttribute

" contenttype attribute
syn match ContenttypeAttribute /contenttype\ze=/ contained
syn match ContenttypeAttributeGroup /contenttype="[^"]*"/ contained contains=String,ContenttypeAttribute

" context attribute
syn match ContextAttribute /context\ze=/ contained
syn match ContextAttributeGroup /context="[^"]*"/ contained contains=String,ContextAttribute

" convert attribute
syn match ConvertAttribute /convert\ze=/ contained
syn match ConvertAttributeGroup /convert="[^"]*"/ contained contains=String,ConvertAttribute

" date attribute
syn match DateAttribute /date\ze=/ contained
syn match DateAttributeGroup /date="[^"]*"/ contained contains=String,DateAttribute

" dateformat attribute
syn match DateformatAttribute /dateformat\ze=/ contained
syn match DateformatAttributeGroup /dateformat="[^"]*"/ contained contains=String,DateformatAttribute

" decimalformat attribute
syn match DecimalformatAttribute /decimalformat\ze=/ contained
syn match DecimalformatAttributeGroup /decimalformat="[^"]*"/ contained contains=String,DecimalformatAttribute

" decoding attribute
syn match DecodingAttribute /decoding\ze=/ contained
syn match DecodingAttributeGroup /decoding="[^"]*"/ contained contains=String,DecodingAttribute

" decrypt attribute
syn match DecryptAttribute /decrypt\ze=/ contained
syn match DecryptAttributeGroup /decrypt="[^"]*"/ contained contains=String,EncryptAttribute

" default attribute
syn match DefaultAttribute /default\ze=/ contained
syn match DefaultAttributeGroup /default="[^"]*"/ contained contains=String,DefaultAttribute

" delete attribute
syn match DeleteAttribute /delete\ze=/ contained
syn match DeleteAttributeGroup /delete="[^"]*"/ contained contains=String,DeleteAttribute

" disabled attribute
syn match DisabledAttribute /disabled\ze=/ contained
syn match DisabledAttributeGroup /disabled="[^"]*"/ contained contains=String,DisabledAttribute

" dynamics attribute
syn match DynamicsAttribute /dynamics\ze=/ contained
syn match DynamicsAttributeGroup /dynamics="[^"]*"/ contained contains=String,DynamicsAttribute

" element attribute
syn match ElementAttribute /element\ze=/ contained
syn match ElementAttributeGroup /element="[^"]*"/ contained contains=String,ElementAttribute

" encoding attribute
syn match EncodingAttribute /encoding\ze=/ contained
syn match EncodingAttributeGroup /encoding="[^"]*"/ contained contains=String,EncodingAttribute

" encrypt attribute
syn match EncryptAttribute /encrypt\ze=/ contained
syn match EncryptAttributeGroup /encrypt="[^"]*"/ contained contains=String,EncryptAttribute

" enctype attribute
syn match EnctypeAttribute /enctype\ze=/ contained
syn match EnctypeAttributeGroup /enctype="[^"]*"/ contained contains=String,EnctypeAttribute

" eq attribute
syn match EqAttribute /eq\ze=/ contained
syn match EqAttributeGroup /eq="[^"]*"/ contained contains=String,EqAttribute

" excerpt attribute
syn match ExcerptAttribute /excerpt\ze=/ contained
syn match ExcerptAttributeGroup /excerpt="[^"]*"/ contained contains=String,ExcerptAttribute

" expression attribute
syn match ExpressionAttribute /expression\ze=/ contained
syn match ExpressionAttributeGroup /expression="[^"]*"/ contained contains=String,ExpressionAttribute

" filter attribute
syn match FilterAttribute /filter\ze=/ contained
syn match FilterAttributeGroup /filter="[^"]*"/ contained contains=String,FilterAttribute

" filterattribute attribute
syn match FilterattributeAttribute /filterattribute\ze=/ contained
syn match FilterattributeAttributeGroup /filterattribute="[^"]*"/ contained contains=String,FilterattributeAttribute

" filteric attribute
syn match FiltericAttribute /filteric\ze=/ contained
syn match FiltericAttributeGroup /filteric="[^"]*"/ contained contains=String,FiltericAttribute

" filterinvert attribute
syn match FilterinvertAttribute /filterinvert\ze=/ contained
syn match FilterinvertAttributeGroup /filterinvert="[^"]*"/ contained contains=String,FilterinvertAttribute

" filtermode attribute
syn match FiltermodeAttribute /filtermode\ze=/ contained
syn match FiltermodeAttributeGroup /filtermode="[^"]*"/ contained contains=String,FiltermodeAttribute

" filterquery attribute
syn match FilterqueryAttribute /filterquery\ze=/ contained
syn match FilterqueryAttributeGroup /filterquery="[^"]*"/ contained contains=String,FilterqueryAttribute

" fixvalue attribute
syn match FixvalueAttribute /fixvalue\ze=/ contained
syn match FixvalueAttributeGroup /fixvalue="[^"]*"/ contained contains=String,FixvalueAttribute

" focalpoint attribute
syn match FocalpointAttribute /focalpoint\ze=/ contained
syn match FocalpointAttributeGroup /focalpoint="[^"]*"/ contained contains=String,FocalpointAttribute

" font attribute
syn match FontAttribute /font\ze=/ contained
syn match FontAttributeGroup /font="[^"]*"/ contained contains=String,FontAttribute

" font-size attribute
syn match Font_sizeAttribute /font-size\ze=/ contained
syn match Font_sizeAttributeGroup /font-size="[^"]*"/ contained contains=String,Font_sizeAttribute

" font-weight attribute
syn match Font_weightAttribute /font-weight\ze=/ contained
syn match Font_weightAttributeGroup /font-weight="[^"]*"/ contained contains=String,Font_weightAttribute

" font2 attribute
syn match Font2Attribute /font2\ze=/ contained
syn match Font2AttributeGroup /font2="[^"]*"/ contained contains=String,Font2Attribute

" fontcolor attribute
syn match FontcolorAttribute /fontcolor\ze=/ contained
syn match FontcolorAttributeGroup /fontcolor="[^"]*"/ contained contains=String,FontcolorAttribute

" fontname attribute
syn match FontnameAttribute /fontname\ze=/ contained
syn match FontnameAttributeGroup /fontname="[^"]*"/ contained contains=String,FontnameAttribute

" fontsize attribute
syn match FontsizeAttribute /fontsize\ze=/ contained
syn match FontsizeAttributeGroup /fontsize="[^"]*"/ contained contains=String,FontsizeAttribute

" fontsize2 attribute
syn match Fontsize2Attribute /fontsize2\ze=/ contained
syn match Fontsize2AttributeGroup /fontsize2="[^"]*"/ contained contains=String,Fontsize2Attribute

" fontstyle attribute
syn match FontstyleAttribute /fontstyle\ze=/ contained
syn match FontstyleAttributeGroup /fontstyle="[^"]*"/ contained contains=String,FontstyleAttribute

" fontweight attribute
syn match FontweightAttribute /fontweight\ze=/ contained
syn match FontweightAttributeGroup /fontweight="[^"]*"/ contained contains=String,FontweightAttribute

" fontweight2 attribute
syn match Fontweight2Attribute /fontweight2\ze=/ contained
syn match Fontweight2AttributeGroup /fontweight2="[^"]*"/ contained contains=String,Fontweight2Attribute

" form attribute
syn match FormAttribute /form\ze=/ contained
syn match FormAttributeGroup /form="[^"]*"/ contained contains=String,FormAttribute

" format attribute
syn match FormatAttribute /format\ze=/ contained
syn match FormatAttributeGroup /format="[^"]*"/ contained contains=String,FormatAttribute

" from attribute
syn match FromAttribute /from\ze=/ contained
syn match FromAttributeGroup /from="[^"]*"/ contained contains=String,FromAttribute

" gravity attribute
syn match GravityAttribute /gravity\ze=/ contained
syn match GravityAttributeGroup /gravity="[^"]*"/ contained contains=String,GravityAttribute

" gt attribute
syn match GtAttribute /gt\ze=/ contained
syn match GtAttributeGroup /gt="[^"]*"/ contained contains=String,GtAttribute

" gte attribute
syn match GteAttribute /gte\ze=/ contained
syn match GteAttributeGroup /gte="[^"]*"/ contained contains=String,GteAttribute

" gui attribute
syn match GuiAttribute /gui\ze=/ contained
syn match GuiAttributeGroup /gui="[^"]*"/ contained contains=String,GuiAttribute

" handler attribute
syn match HandlerAttribute /handler\ze=/ contained
syn match HandlerAttributeGroup /handler="[^"]*"/ contained contains=String,HandlerAttribute

" height attribute
syn match HeightAttribute /height\ze=/ contained
syn match HeightAttributeGroup /height="[^"]*"/ contained contains=String,HeightAttribute

" hidden attribute
syn match HiddenAttribute /hidden\ze=/ contained
syn match HiddenAttributeGroup /hidden="[^"]*"/ contained contains=String,HiddenAttribute

" hide attribute
syn match HideAttribute /hide\ze=/ contained
syn match HideAttributeGroup /hide="[^"]*"/ contained contains=String,HideAttribute

" hyphenEditor attribute
syn match HyphenEditorAttribute /hyphenEditor\ze=/ contained
syn match HyphenEditorAttributeGroup /hyphenEditor="[^"]*"/ contained contains=String,HyphenEditorAttribute

" ic attribute
syn match IcAttribute /ic\ze=/ contained
syn match IcAttributeGroup /ic="[^"]*"/ contained contains=String,IcAttribute

" id attribute
syn match IdAttribute /id\ze=/ contained
syn match IdAttributeGroup /id="[^"]*"/ contained contains=String,IdAttribute

" image attribute
syn match ImageAttribute /image\ze=/ contained
syn match ImageAttributeGroup /image="[^"]*"/ contained contains=String,ImageAttribute

" indent attribute
syn match IndentAttribute /indent\ze=/ contained
syn match IndentAttributeGroup /indent="[^"]*"/ contained contains=String,IndentAttribute

" index attribute
syn match IndexAttribute /index\ze=/ contained
syn match IndexAttributeGroup /index="[^"]*"/ contained contains=String,IndexAttribute

" information attribute
syn match InformationAttribute /information\ze=/ contained
syn match InformationAttributeGroup /information="[^"]*"/ contained contains=String,InformationAttribute

" informationID attribute
syn match InformationIDAttribute /informationID\ze=/ contained
syn match InformationIDAttributeGroup /informationID="[^"]*"/ contained contains=String,InformationIDAttribute

" insert attribute
syn match InsertAttribute /insert\ze=/ contained
syn match InsertAttributeGroup /insert="[^"]*"/ contained contains=String,InsertAttribute

" invert attribute
syn match InvertAttribute /invert\ze=/ contained
syn match InvertAttributeGroup /invert="[^"]*"/ contained contains=String,InvertAttribute

" isNull attribute
syn match IsNullAttribute /isNull\ze=/ contained
syn match IsNullAttributeGroup /isNull="[^"]*"/ contained contains=String,IsNullAttribute

" item attribute
syn match ItemAttribute /item\ze=/ contained
syn match ItemAttributeGroup /item="[^"]*"/ contained contains=String,ItemAttribute

" itemtext attribute
syn match ItemtextAttribute /itemtext\ze=/ contained
syn match ItemtextAttributeGroup /itemtext="[^"]*"/ contained contains=String,ItemtextAttribute

" key attribute
syn match KeyAttribute /key\ze=/ contained
syn match KeyAttributeGroup /key="[^"]*"/ contained contains=String,KeyAttribute

" keys attribute
syn match KeysAttribute /keys\ze=/ contained
syn match KeysAttributeGroup /keys="[^"]*"/ contained contains=String,KeysAttribute

" language attribute
syn match LanguageAttribute /language\ze=/ contained
syn match LanguageAttributeGroup /language="[^"]*"/ contained contains=String,LanguageAttribute

" layout attribute
syn match Text_transformAttribute /layout\ze=/ contained
syn match Text_transformAttributeGroup /layout="[^"]*"/ contained contains=String,Text_transformAttribute

" leaflink attribute
syn match LeaflinkAttribute /leaflink\ze=/ contained
syn match LeaflinkAttributeGroup /leaflink="[^"]*"/ contained contains=String,LeaflinkAttribute

" level attribute
syn match LevelAttribute /level\ze=/ contained
syn match LevelAttributeGroup /level="[^"]*"/ contained contains=String,LevelAttribute

" linkcolor attribute
syn match LinkcolorAttribute /linkcolor\ze=/ contained
syn match LinkcolorAttributeGroup /linkcolor="[^"]*"/ contained contains=String,LinkcolorAttribute

" list attribute
syn match ListAttribute /list\ze=/ contained
syn match ListAttributeGroup /list="[^"]*"/ contained contains=String,ListAttribute

" locale attribute
syn match LocaleAttribute /locale\ze=/ contained
syn match LocaleAttributeGroup /locale="[^"]*"/ contained contains=String,LocaleAttribute

" localelink attribute
syn match LocalelinkAttribute /localelink\ze=/ contained
syn match LocalelinkAttributeGroup /localelink="[^"]*"/ contained contains=String,LocalelinkAttribute

" login attribute
syn match LoginAttribute /login\ze=/ contained
syn match LoginAttributeGroup /login="[^"]*"/ contained contains=String,LoginAttribute

" lookup attribute
syn match LookupAttribute /lookup\ze=/ contained
syn match LookupAttributeGroup /lookup="[^"]*"/ contained contains=String,LookupAttribute

" lt attribute
syn match LtAttribute /lt\ze=/ contained
syn match LtAttributeGroup /lt="[^"]*"/ contained contains=String,LtAttribute

" lte attribute
syn match LteAttribute /lte\ze=/ contained
syn match LteAttributeGroup /lte="[^"]*"/ contained contains=String,LteAttribute

" manipulate attribute
syn match ManipulateAttribute /manipulate\ze=/ contained
syn match ManipulateAttributeGroup /manipulate="[^"]*"/ contained contains=String,ManipulateAttribute

" match attribute
syn match MatchAttribute /match\ze=/ contained
syn match MatchAttributeGroup /match="[^"]*"/ contained contains=String,MatchAttribute

" max attribute
syn match MaxAttribute /max\ze=/ contained
syn match MaxAttributeGroup /max="[^"]*"/ contained contains=String,MaxAttribute

" method attribute
syn match MethodAttribute /method\ze=/ contained
syn match MethodAttributeGroup /method="[^"]*"/ contained contains=String,MethodAttribute

" min attribute
syn match MinAttribute /min\ze=/ contained
syn match MinAttributeGroup /min="[^"]*"/ contained contains=String,MinAttribute

" mode attribute
syn match ModeAttribute /mode\ze=/ contained
syn match ModeAttributeGroup /mode="[^"]*"/ contained contains=String,ModeAttribute

" module attribute
syn match ModuleAttribute /module\ze=/ contained
syn match ModuleAttributeGroup /module="[^"]*"/ contained contains=String,ModuleAttribute

" multiple attribute
syn match MultipleAttribute /multiple\ze=/ contained
syn match MultipleAttributeGroup /multiple="[^"]*"/ contained contains=String,MultipleAttribute

" name attribute
syn match NameAttribute /name\ze=/ contained
syn match NameAttributeGroup /name="[^"]*"/ contained contains=String,NameAttribute

" nameencoding attribute
syn match NameencodingAttribute /nameencoding\ze=/ contained
syn match NameencodingAttributeGroup /nameencoding="[^"]*"/ contained contains=String,NameencodingAttribute

" neq attribute
syn match NeqAttribute /neq\ze=/ contained
syn match NeqAttributeGroup /neq="[^"]*"/ contained contains=String,NeqAttribute

" node attribute
syn match NodeAttribute /node\ze=/ contained
syn match NodeAttributeGroup /node="[^"]*"/ contained contains=String,NodeAttribute

" nowButton attribute
syn match NowButtonAttribute /nowButton\ze=/ contained
syn match NowButtonAttributeGroup /nowButton="[^"]*"/ contained contains=String,NowButtonAttribute

" object attribute
syn match ObjectAttribute /object\ze=/ contained
syn match ObjectAttributeGroup /object="[^"]*"/ contained contains=String,ObjectAttribute

" objekt attribute
syn match ObjektAttribute /objekt\ze=/ contained
syn match ObjektAttributeGroup /objekt="[^"]*"/ contained contains=String,ObjektAttribute

" offset attribute
syn match OffsetAttribute /offset\ze=/ contained
syn match OffsetAttributeGroup /offset="[^"]*"/ contained contains=String,OffsetAttribute

" onclick attribute
syn match OnclickAttribute /onclick\ze=/ contained
syn match OnclickAttributeGroup /onclick="[^"]*"/ contained contains=String,OnclickAttribute

" options attribute
syn match OptionsAttribute /options\ze=/ contained
syn match OptionsAttributeGroup /options="[^"]*"/ contained contains=String,OptionsAttribute

" overwrite attribute
syn match OverwriteAttribute /overwrite\ze=/ contained
syn match OverwriteAttributeGroup /overwrite="[^"]*"/ contained contains=String,OverwriteAttribute

" padding attribute
syn match PaddingAttribute /padding\ze=/ contained
syn match PaddingAttributeGroup /padding="[^"]*"/ contained contains=String,PaddingAttribute

" paddingcolor attribute
syn match PaddingcolorAttribute /paddingcolor\ze=/ contained
syn match PaddingcolorAttributeGroup /paddingcolor="[^"]*"/ contained contains=String,PaddingcolorAttribute

" parentlink attribute
syn match ParentlinkAttribute /parentlink\ze=/ contained
syn match ParentlinkAttributeGroup /parentlink="[^"]*"/ contained contains=String,ParentlinkAttribute

" password attribute
syn match PasswordAttribute /password\ze=/ contained
syn match PasswordAttributeGroup /password="[^"]*"/ contained contains=String,PasswordAttribute

" poolID attribute
syn match PoolIdAttribute /poolID\ze=/ contained
syn match PoolIdAttributeGroup /poolID="[^"]*"/ contained contains=String,PoolIdAttribute

" pools attribute
syn match PoolsAttribute /pools\ze=/ contained
syn match PoolsAttributeGroup /pools="[^"]*"/ contained contains=String,PoolsAttribute

" popupheight attribute
syn match PopupheightAttribute /popupheight\ze=/ contained
syn match PopupheightAttributeGroup /popupheight="[^"]*"/ contained contains=String,PopupheightAttribute

" popupwidth attribute
syn match PopupwidthAttribute /popupwidth\ze=/ contained
syn match PopupwidthAttributeGroup /popupwidth="[^"]*"/ contained contains=String,PopupwidthAttribute

" previewimage attribute
syn match PreviewimageAttribute /previewimage\ze=/ contained
syn match PreviewimageAttributeGroup /previewimage="[^"]*"/ contained contains=String,PreviewimageAttribute

" publisher attribute
syn match PublisherAttribute /publisher\ze=/ contained
syn match PublisherAttributeGroup /publisher="[^"]*"/ contained contains=String,PublisherAttribute

" quality attribute
syn match QualityAttribute /quality\ze=/ contained
syn match QualityAttributeGroup /quality="[^"]*"/ contained contains=String,QualityAttribute

" query attribute
syn match QueryAttribute /query\ze=/ contained
syn match QueryAttributeGroup /query="[^"]*"/ contained contains=String,QueryAttribute

" querystring attribute
syn match QuerystringAttribute /querystring\ze=/ contained
syn match QuerystringAttributeGroup /querystring="[^"]*"/ contained contains=String,QuerystringAttribute

" range attribute
syn match RangeAttribute /range\ze=/ contained
syn match RangeAttributeGroup /range="[^"]*"/ contained contains=String,RangeAttribute

" readonly attribute
syn match ReadonlyAttribute /readonly\ze=/ contained
syn match ReadonlyAttributeGroup /readonly="[^"]*"/ contained contains=String,ReadonlyAttribute

" return attribute
syn match ReturnAttribute /return\ze=/ contained
syn match ReturnAttributeGroup /return="[^"]*"/ contained contains=String,ReturnAttribute

" role attribute
syn match RoleAttribute /role\ze=/ contained
syn match RoleAttributeGroup /role="[^"]*"/ contained contains=String,RoleAttribute

" rootElement attribute
syn match RootElementAttribute /rootElement\ze=/ contained
syn match RootElementAttributeGroup /rootElement="[^"]*"/ contained contains=String,RootElementAttribute

" rootelement attribute
syn match RootelementAttribute /rootelement\ze=/ contained
syn match RootelementAttributeGroup /rootelement="[^"]*"/ contained contains=String,RootelementAttribute

" rows attribute
syn match RowsAttribute /rows\ze=/ contained
syn match RowsAttributeGroup /rows="[^"]*"/ contained contains=String,RowsAttribute

" scalesteps attribute
syn match ScalestepsAttribute /scalesteps\ze=/ contained
syn match ScalestepsAttributeGroup /scalesteps="[^"]*"/ contained contains=String,ScalestepsAttribute

" scope attribute
syn match ScopeAttribute /scope\ze=/ contained
syn match ScopeAttributeGroup /scope="[^"]*"/ contained contains=String,ScopeAttribute

" selected attribute
syn match SelectedAttribute /selected\ze=/ contained
syn match SelectedAttributeGroup /selected="[^"]*"/ contained contains=String,SelectedAttribute

" separator attribute
syn match SeparatorAttribute /separator\ze=/ contained
syn match SeparatorAttributeGroup /separator="[^"]*"/ contained contains=String,SeparatorAttribute

" sequences attribute
syn match SequencesAttribute /sequences\ze=/ contained
syn match SequencesAttributeGroup /sequences="[^"]*"/ contained contains=String,SequencesAttribute

" session attribute
syn match SessionAttribute /session\ze=/ contained
syn match SessionAttributeGroup /session="[^"]*"/ contained contains=String,SessionAttribute

" showtree attribute
syn match ShowtreeAttribute /showtree\ze=/ contained
syn match ShowtreeAttributeGroup /showtree="[^"]*"/ contained contains=String,ShowtreeAttribute

" showTree attribute
syn match ShowtreeDeprecatedAttribute /showTree\ze=/ contained
syn match ShowtreeDeprecatedAttributeGroup /showTree="[^"]*"/ contained contains=String,ShowtreeDeprecatedAttribute

" size attribute
syn match SizeAttribute /size\ze=/ contained
syn match SizeAttributeGroup /size="[^"]*"/ contained contains=String,SizeAttribute

" sortkeys attribute
syn match SortkeysAttribute /sortkeys\ze=/ contained
syn match SortkeysAttributeGroup /sortkeys="[^"]*"/ contained contains=String,SortkeysAttribute

" sortsequences attribute
syn match SortsequencesAttribute /sortsequences\ze=/ contained
syn match SortsequencesAttributeGroup /sortsequences="[^"]*"/ contained contains=String,SortsequencesAttribute

" sorttypes attribute
syn match SorttypesAttribute /sorttypes\ze=/ contained
syn match SorttypesAttributeGroup /sorttypes="[^"]*"/ contained contains=String,SorttypesAttribute

" source attribute
syn match SourceAttribute /source\ze=/ contained
syn match SourceAttributeGroup /source="[^"]*"/ contained contains=String,SourceAttribute

" step attribute
syn match StepAttribute /step\ze=/ contained
syn match StepAttributeGroup /step="[^"]*"/ contained contains=String,StepAttribute

" style attribute
syn match StyleAttribute /style\ze=/ contained
syn match StyleAttributeGroup /style="[^"]*"/ contained contains=String,StyleAttribute

" template attribute
syn match TemplateAttribute /template\ze=/ contained
syn match TemplateAttributeGroup /template="[^"]*"/ contained contains=String,TemplateAttribute

" text attribute
syn match TextAttribute /text\ze=/ contained
syn match TextAttributeGroup /text="[^"]*"/ contained contains=String,TextAttribute

" text-transform attribute
syn match Text_transformAttribute /text-transform\ze=/ contained
syn match Text_transformAttributeGroup /text-transform="[^"]*"/ contained contains=String,Text_transformAttribute

" textlabel attribute
syn match TextlabelAttribute /textlabel\ze=/ contained
syn match TextlabelAttributeGroup /textlabel="[^"]*"/ contained contains=String,TextlabelAttribute

" theme attribute
syn match ThemeAttribute /theme\ze=/ contained
syn match ThemeAttributeGroup /theme="[^"]*"/ contained contains=String,ThemeAttribute

" title attribute
syn match TitleAttribute /title\ze=/ contained
syn match TitleAttributeGroup /title="[^"]*"/ contained contains=String,TitleAttribute

" to attribute
syn match ToAttribute /to\ze=/ contained
syn match ToAttributeGroup /to="[^"]*"/ contained contains=String,ToAttribute

" toggle attribute
syn match ToggleAttribute /toggle\ze=/ contained
syn match ToggleAttributeGroup /toggle="[^"]*"/ contained contains=String,ToggleAttribute

" transform attribute
syn match TransformAttribute /transform\ze=/ contained
syn match TransformAttributeGroup /transform="[^"]*"/ contained contains=String,TransformAttribute

" type attribute
syn match TypeAttribute /type\ze=/ contained
syn match TypeAttributeGroup /type="[^"]*"/ contained contains=String,TypeAttribute

" types attribute
syn match TypesAttribute /types\ze=/ contained
syn match TypesAttributeGroup /types="[^"]*"/ contained contains=String,TypesAttribute

" uri attribute
syn match UriAttribute /uri\ze=/ contained
syn match UriAttributeGroup /uri="[^"]*"/ contained contains=String,UriAttribute

" url attribute
syn match UrlAttribute /url\ze=/ contained
syn match UrlAttributeGroup /url="[^"]*"/ contained contains=String,UrlAttribute

" urlonly attribute
syn match UrlonlyAttribute /urlonly\ze=/ contained
syn match UrlonlyAttributeGroup /urlonly="[^"]*"/ contained contains=String,UrlonlyAttribute

" urlparam attribute
syn match UrlparamAttribute /urlparam\ze=/ contained
syn match UrlparamAttributeGroup /urlparam="[^"]*"/ contained contains=String,UrlparamAttribute

" user attribute
syn match UserAttribute /user\ze=/ contained
syn match UserAttributeGroup /user="[^"]*"/ contained contains=String,UserAttribute

" value attribute
syn match ValueAttribute /value\ze=/ contained
syn match ValueAttributeGroup /value="[^"]*"/ contained contains=String,ValueAttribute

" varName attribute
syn match VarNameAttribute /varName\ze=/ contained
syn match VarNameAttributeGroup /varName="[^"]*"/ contained contains=String,VarNameAttribute

" varname attribute
syn match VarnameAttribute /varname\ze=/ contained
syn match VarnameAttributeGroup /varname="[^"]*"/ contained contains=String,VarnameAttribute

" width attribute
syn match WidthAttribute /width\ze=/ contained
syn match WidthAttributeGroup /width="[^"]*"/ contained contains=String,WidthAttribute

" window attribute
syn match WindowAttribute /window\ze=/ contained
syn match WindowAttributeGroup /window="[^"]*"/ contained contains=String,WindowAttribute

" worklistID attribute
syn match WorklistIDAttribute /worklistID\ze=/ contained
syn match WorklistIDAttributeGroup /worklistID="[^"]*"/ contained contains=String,WorklistIDAttribute

" sp-tags

" sp:argument
syn region ArgumentSPTag start=/<[\/]\?sp:argument\>/ms=e-10 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,ExpressionAttributeGroup,ConditionAttributeGroup,ObjectAttributeGroup,DefaultAttributeGroup,LocaleAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=ArgumentSPTag

" sp:attribute
syn region AttributeSPTag start=/<[\/]\?sp:attribute\>/ms=e-11 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,TextAttributeGroup,ObjectAttributeGroup,DynamicsAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=AttributeSPTag

" sp:barcode
syn region BarcodeSPTag start=/<[\/]\?sp:barcode\>/ms=e-9 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,TextAttributeGroup,TypeAttributeGroup,WidthAttributeGroup,HeightAttributeGroup,LocaleAttributeGroup,ScopeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=BarcodeSPTag

" sp:break
syn region BreakSPTag start=/<[\/]\?sp:break\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=AttributeErrorGroup fold
sy cluster SPTag add=BreakSPTag

" sp:calendarsheet
syn region CalendarSheetSPTag start=/<[\/]\?sp:calendarSheet\>/ms=e-15 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ActionAttributeGroup,ValueAttributeGroup,ObjectAttributeGroup,DateAttributeGroup,FromAttributeGroup,ToAttributeGroup,ModeAttributeGroup,ScopeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=CalendarSheetSPTag

" sp:checkbox
syn region CheckboxSPTag start=/<[\/]\?sp:checkbox\>/ms=e-10 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,LocaleAttributeGroup,TypeAttributeGroup,CheckedAttributeGroup,DisabledAttributeGroup,ReadonlyAttributeGroup,AttributeGroup fold
sy cluster SPTag add=CheckboxSPTag

" sp:code
syn region CodeSPTag start=/<[\/]\?sp:code\>/ms=e-6 end=/[\/]\?>/me=s-1 contains=AttributeErrorGroup fold
sy cluster SPTag add=CodeSPTag

" sp:collection
syn region CollectionSPTag start=/<[\/]\?sp:collection\>/ms=e-12 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ActionAttributeGroup,ValueAttributeGroup,ObjectAttributeGroup,ExpressionAttributeGroup,ConditionAttributeGroup,IndexAttributeGroup,QueryAttributeGroup,DefaultAttributeGroup,PublisherAttributeGroup,LocaleAttributeGroup,ScopeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=CollectionSPTag

" sp:condition
syn region ConditionSPTag start=/<[\/]\?sp:condition\>/ms=e-11 end=/[\/]\?>/me=s-1 contains=AttributeErrorGroup fold
sy cluster SPTag add=ConditionSPTag

" sp:diff
syn region DiffSPTag start=/<[\/]\?sp:diff\>/ms=e-6 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,FromAttributeGroup,ToAttributeGroup,LocaleAttributeGroup,LookupAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=DiffSPTag

" sp:else
syn region ElseSPTag start=/<[\/]\?sp:else\>/ms=e-6 end=/[\/]\?>/me=s-1 contains=AttributeErrorGroup fold
sy cluster SPTag add=ElseSPTag

" sp:elseif
syn region ElseifSPTag start=/<[\/]\?sp:elseif\>/ms=e-8 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,LocaleAttributeGroup,IsNullAttributeGroup,IcAttributeGroup,ContainsAttributeGroup,MatchAttributeGroup,EqAttributeGroup,NeqAttributeGroup,GtAttributeGroup,GteAttributeGroup,LtAttributeGroup,LteAttributeGroup,ConditionAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=ElseifSPTag

" sp:error
syn region ErrorSPTag start=/<[\/]\?sp:error\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=CodeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=ErrorSPTag

" sp:expire
syn region ExpireSPTag start=/<[\/]\?sp:expire\>/ms=e-8 end=/[\/]\?>/me=s-1 contains=DateAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=ExpireSPTag

" sp:filter
syn region FilterSPTag start=/<[\/]\?sp:filter\>/ms=e-8 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,CollectionAttributeGroup,AttributeAttributeGroup,FilterAttributeGroup,IcAttributeGroup,InvertAttributeGroup,ModeAttributeGroup,LocaleAttributeGroup,FromAttributeGroup,ToAttributeGroup,TypeAttributeGroup,FormatAttributeGroup,ScopeAttributeGroup,AttributeGroup fold
sy cluster SPTag add=FilterSPTag

" sp:for
syn region ForSPTag start=/<[\/]\?sp:for\>/ms=e-5 end=/[\/]\?>/me=s-1 contains=IndexAttributeGroup,FromAttributeGroup,ToAttributeGroup,ConditionAttributeGroup,StepAttributeGroup,LocaleAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=ForSPTag

" sp:form
syn region FormSPTag start=/<[\/]\?sp:form\>/ms=e-6 end=/[\/]\?>/me=s-1 contains=UriAttributeGroup,TemplateAttributeGroup,HandlerAttributeGroup,ModuleAttributeGroup,ContextAttributeGroup,NameencodingAttributeGroup,EnctypeAttributeGroup,MethodeAttributeGroup,IdAttributeGroup,NameAttributeGroup,CommandAttributeGroup,LocaleAttributeGroup,AttributeGroup fold
sy cluster SPTag add=FormSPTag

" sp:hidden
syn region HiddenSPTag start=/<[\/]\?sp:hidden\>/ms=e-8 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,FixvalueAttributeGroup,LocaleAttributeGroup,TypeAttributeErrorGroup fold
sy cluster SPTag add=HiddenSPTag

" sp:if
syn region IfSPTag start=/<[\/]\?sp:if\>/ms=e-4 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,LocaleAttributeGroup,IsNullAttributeGroup,IcAttributeGroup,ContainsAttributeGroup,MatchAttributeGroup,EqAttributeGroup,NeqAttributeGroup,GtAttributeGroup,GteAttributeGroup,LtAttributeGroup,LteAttributeGroup,ConditionAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=IfSPTag

" sp:include
syn region IncludeSPTag start=/<[\/]\?sp:include\>/ms=e-9 end=/[\/]\?>/me=s-1 contains=TemplateAttributeGroup,AnchorAttributeGroup,UriAttributeGroup,ContextAttributeGroup,ModuleAttributeGroup,ModeAttributeGroup,ArgumentsAttributeGroup,ReturnAttributeGroup,ArgumentErrorGroup fold
sy cluster SPTag add=IncludeSPTag

" sp:io
syn region IoSPTag start=/<[\/]\?sp:io\>/ms=e-4 end=/[\/]\?>/me=s-1 contains=TypeAttributeGroup,ContenttypeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=IoSPTag

" sp:iterator
syn region IteratorSPTag start=/<[\/]\?sp:iterator\>/ms=e-10 end=/[\/]\?>/me=s-1 contains=CollectionAttributeGroup,ItemAttributeGroup,MinAttributeGroup,MaxAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=IteratorSPTag

" sp:json
syn region JsonSPTag start=/<[\/]\?sp:json\>/ms=e-6 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ObjectAttributeGroup,IndentAttributeGroup,OverwriteAttributeGroup,LocaleAttributeGroup,ScopeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=JsonSPTag

" sp:linktree
syn region LinktreeSPTag start=/<[\/]\?sp:linktree\>/ms=e-10 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,RootelementAttributeGroup,AttributesAttributeGroup,ParentlinkAttributeGroup,LocalelinkAttributeGroup,SortkeysAttributeGroup,SortsequencesAttributeGroup,SorttypesAttributeGroup,LocaleAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=LinktreeSPTag

" sp:livetree
syn region LivetreeSPTag start=/<[\/]\?sp:livetree\>/ms=e-10 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ActionAttributeGroup,NodeAttributeGroup,RootElementAttributeGroup,PublisherAttributeGroup,ParentlinkAttributeGroup,LeaflinkAttributeGroup,SortkeysAttributeGroup,SortsequencesAttributeGroup,SorttypesAttributeGroup,LocaleAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=LivetreeSPTag

" sp:log
syn region LogSPTag start=/<[\/]\?sp:log\>/ms=e-5 end=/[\/]\?>/me=s-1 contains=LevelAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=LogSPTag

" sp:login
syn region LoginSPTag start=/<[\/]\?sp:login\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=SessionAttributeGroup,LoginAttributeGroup,PasswordAttributeGroup,ClientAttributeGroup,ScopeAttributeGroup,LocaleAttributeGroup,CaptcharequiredAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=LoginSPTag

" sp:loop
syn region LoopSPTag start=/<[\/]\?sp:loop\>/ms=e-6 end=/[\/]\?>/me=s-1 contains=CollectionAttributeGroup,ListAttributeGroup,ItemAttributeGroup,SeparatorAttributeGroup,LocaleAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=LoopSPTag

" sp:map
syn region MapSPTag start=/<[\/]\?sp:map\>/ms=e-5 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ActionAttributeGroup,KeyAttributeGroup,ValueAttributeGroup,ObjectAttributeGroup,ExpressionAttributeGroup,ConditionAttributeGroup,DefaultAttributeGroup,LocaleAttributeGroup,ScopeAttributeGroup,OverwriteAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=MapSPTag

" sp:option
syn region OptionSPTag start=/<[\/]\?sp:option\>/ms=e-8 end=/[\/]\?>/me=s-1 contains=ValueAttributeGroup,DisabledAttributeGroup,SelectedAttributeGroup,LocaleAttributeGroup fold
sy cluster SPTag add=OptionSPTag

" sp:print
syn region PrintSPTag start=/<[\/]\?sp:print\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,TextAttributeGroup,ExpressionAttributeGroup,ConditionAttributeGroup,DefaultAttributeGroup,ConvertAttributeGroup,EncodingAttributeGroup,DecodingAttributeGroup,EncryptAttributeGroup,DecryptAttributeGroup,LocaleAttributeGroup,DateformatAttributeGroup,DecimalFormatAttributeGroup,ArgAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=PrintSPTag

" sp:radio
syn region RadioSPTag start=/<[\/]\?sp:radio\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,LocaleAttributeGroup,TypeAttributeGroup,CheckedAttributeGroup,DisabledAttributeGroup,ReadonlyAttributeGroup fold
sy cluster SPTag add=RadioSPTag

" sp:range
syn region RangeSPTag start=/<[\/]\?sp:range\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,CollectionAttributeGroup,RangeAttributeGroup,ScopeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=RangeSPTag

" sp:return
syn region ReturnSPTag start=/<[\/]\?sp:return\>/ms=e-8 end=/[\/]\?>/me=s-1 contains=ValueAttributeGroup,ExpressionAttributeGroup,ConditionAttributeGroup,ObjectAttributeGroup,DefaultAttributeGroup,LocaleAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=ReturnSPTag

" sp:scaleimage
syn region ScaleimageSPTag start=/<[\/]\?sp:scaleimage\>/ms=e-12 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ObjectAttributeGroup,WidthAttributeGroup,HeightAttributeGroup,PaddingAttributeGroup,BackgroundAttributeGroup,QualityAttributeGroup,ScalestepsAttributeGroup,OptionsAttributeGroup,LocaleAttributeGroup,ScopeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=ScaleimageSPTag

" sp:scope
syn region ScopeSPTag start=/<[\/]\?sp:scope\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=ScopeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=ScopeSPTag

" sp:select
syn region SelectSPTag start=/<[\/]\?sp:select\>/ms=e-8 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,LocaleAttributeGroup,TypeAttributeGroup,DisabledAttributeGroup,MultipleAttributeGroup,AttributeGroup fold
sy cluster SPTag add=SelectSPTag

" sp:set
syn region SetSPTag start=/<[\/]\?sp:set\>/ms=e-5 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,ExpressionAttributeGroup,ConditionAttributeGroup,ObjectAttributeGroup,DefaultAttributeGroup,OverwriteAttributeGroup,LocaleAttributeGroup,ScopeAttributeGroup,InsertAttributeGroup,ContentTypeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=SetSPTag

" sp:sort
syn region SortSPTag start=/<[\/]\?sp:sort\>/ms=e-6 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,CollectionAttributeGroup,KeysAttributeGroup,SequencesAttributeGroup,TypesAttributeGroup,LocaleAttributeGroup,ScopeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=SortSPTag

" sp:sass
syn region SassSPTag start=/<[\/]\?sp:sass\>/ms=e-6 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,SourceAttributeGroup,OptionsAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=SassSPTag

" sp:subinformation
syn region SubinformationSPTag start=/<[\/]\?sp:subinformation\>/ms=e-16 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,TypeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=SubinformationSPTag

" sp:tagbody
syn region TagbodySPTag start=/<[\/]\?sp:tagbody\>/ms=e-9 end=/[\/]\?>/me=s-1 contains=AttributeErrorGroup fold
sy cluster SPTag add=TagbodySPTag

" sp:text
syn region TextSPTag start=/<[\/]\?sp:text\>/ms=e-6 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,FixvalueAttributeGroup,LocaleAttributeGroup,TypeAttributeGroup,FormatAttributeGroup,DisabledAttributeGroup,ReadonlyAttributeGroup,InputTypeAttributeGroup,AttributeGroup fold
sy cluster SPTag add=TextSPTag

" sp:textarea
syn region TextareaSPTag start=/<[\/]\?sp:textarea\>/ms=e-10 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,FixvalueAttributeGroup,LocaleAttributeGroup,TypeAttributeGroup,DisabledAttributeGroup,ReadonlyAttributeGroup,AttributeGroup fold
sy cluster SPTag add=TextareaSPTag

" sp:textimage
syn region TextimageSPTag start=/<[\/]\?sp:textimage\>/ms=e-11 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,TextAttributeGroup,WidthAttributeGroup,HeightAttributeGroup,FontnameAttributeGroup,FontsizeAttributeGroup,FontstyleAttributeGroup,FontcolorAttributeGroup,BackgroundAttributeGroup,GravityAttributeGroup,OffsetAttributeGroup,LocaleAttributeGroup,ScopeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=TextimageSPTag

" sp:upload
syn region UploadSPTag start=/<[\/]\?sp:upload\>/ms=e-8 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,LocaleAttributeGroup fold
sy cluster SPTag add=UploadSPTag

" sp:url
syn region UrlSPTag start=/<[\/]\?sp:url\>/ms=e-5 end=/[\/]\?>/me=s-1 contains=UriAttributeGroup,TemplateAttributeGroup,HandlerAttributeGroup,ModuleAttributeGroup,ContextAttributeGroup,WindowAttributeGroup,CommandAttributeGroup,InformationAttributeGroup,PublisherAttributeGroup,AbsoluteAttributeGroup,GuiAttributeGroup,LocaleAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=UrlSPTag

" sp:warning
syn region WarningSPTag start=/<[\/]\?sp:warning\>/ms=e-9 end=/[\/]\?>/me=s-1 contains=CodeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=WarningSPTag

" sp:worklist
syn region WorklistSPTag start=/<[\/]\?sp:worklist\>/ms=e-10 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,RoleAttributeGroup,UserAttributeGroup,ElementAttributeGroup,AttributeErrorGroup fold
sy cluster SPTag add=WorklistSPTag

" spt-tags

" spt:counter
syn region CounterSPTTag start=/<[\/]\?spt:counter\>/ms=e-11 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ModeAttributeGroup,VarNameAttributeGroup,VarnameAttributeGroup,LanguageAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=CounterSPTTag

" spt:date
syn region DateSPTTag start=/<[\/]\?spt:date\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,SizeAttributeGroup,ValueAttributeGroup,FixvalueAttributeGroup,TypeAttributeGroup,NowButtonAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=DateSPTTag

" spt:diff
syn region DiffSPTTag start=/<[\/]\?spt:diff\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=FromAttributeGroup,ToAttributeGroup,StyleAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=DiffSPTTag

" spt:email2img
syn region Email2imgSPTTag start=/<[\/]\?spt:email2img\>/ms=e-12 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ObjectAttributeGroup,AltAttributeGroup,TitleAttributeGroup,ColorAttributeGroup,LinkcolorAttributeGroup,BgcolorAttributeGroup,FontAttributeGroup,FontsizeAttributeGroup,FontweightAttributeGroup,Color2AttributeGroup,BgColor2AttributeGroup,Font2AttributeGroup,Fontsize2AttributeGroup,Fontweight2AttributeGroup,FormAttributeGroup,OnclickAttributeGroup,PopupwidthAttributeGroup,PopupheightAttributeGroup,UrlparamAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=Email2imgSPTTag

" spt:encryptemail
syn region EncryptemailSPTTag start=/<[\/]\?spt:encryptemail\>/ms=e-15 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ObjectAttributeGroup,FormAttributeGroup,PopupwidthAttributeGroup,PopupheightAttributeGroup,UrlparamAttributeGroup,AttributeGroup fold
sy cluster SPTTag add=EncryptemailSPTTag

" spt:escapeemail
syn region EscapeemailSPTTag start=/<[\/]\?spt:escapeemail\>/ms=e-14 end=/[\/]\?>/me=s-1 contains=ObjectAttributeGroup,AltAttributeGroup,TitleAttributeGroup,ColorAttributeGroup,BgcolorAttributeGroup,FontAttributeGroup,FontsizeAttributeGroup,FontweightAttributeGroup,FormAttributeGroup,OnclickAttributeGroup,PopupwidthAttributeGroup,PopupheightAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=EscapeemailSPTTag

" spt:formsolutions
syn region FormsolutionsSPTTag start=/<[\/]\?spt:formsolutions\>/ms=e-16 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,LocaleAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=FormsolutionsSPTTag

" spt:id2url
syn region Id2urlSPTTag start=/<[\/]\?spt:id2url\>/ms=e-9 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ObjektAttributeGroup,QuerystringAttributeGroup,ClassnameAttributeGroup,LocaleAttributeGroup,UrlAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=Id2urlSPTTag

" spt:ilink
syn region IlinkSPTTag start=/<[\/]\?spt:ilink\>/ms=e-8 end=/[\/]\?>/me=s-1 contains=ValueAttributeGroup,InformationAttributeGroup,StepAttributeGroup,ActionAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=IlinkSPTTag

" spt:imageeditor
syn region ImageeditorSPTTag start=/<[\/]\?spt:imageeditor\>/ms=e-14 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ObjectAttributeGroup,FocalpointAttributeGroup,DeleteAttributeGroup,LocaleAttributeGroup,WidthAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=ImageeditorSPTTag

" spt:imp
syn region ImpSPTTag start=/<[\/]\?spt:imp\>/ms=e-6 end=/[\/]\?>/me=s-1 contains=ImageAttributeGroup,WidthAttributeGroup,HeightAttributeGroup,AltAttributeGroup,FormatAttributeGroup,ScalestepsAttributeGroup,BackgroundAttributeGroup,UrlonlyAttributeGroup,PaddingAttributeGroup,PaddingcolorAttributeGroup,ExcerptAttributeGroup,ManipulateAttributeGroup,TextAttributeGroup,FontnameAttributeGroup,FontcolorAttributeGroup,FontsizeAttributeGroup,FontweightAttributeGroup,ColorAttributeGroup,FontAttributeGroup,Font_sizeAttributeGroup,Font_weightAttributeGroup,GravityAttributeGroup,OffsetAttributeGroup,TransformAttributeGroup,Text_transformAttributeGroup,AttributeGroup fold
sy cluster SPTTag add=ImpSPTTag

" spt:iterator
syn region IteratorSPTTag start=/<[\/]\?spt:iterator\>/ms=e-11 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ItemAttributeGroup,MinAttributeGroup,MaxAttributeGroup,LayoutAttributeGroup,ReadonlyAttributeGroup,ItemtextAttributeGroup,InvertAttributeGroup,DisabledAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=IteratorSPTTag

" spt:link
syn region LinkSPTTag start=/<[\/]\?spt:link\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,TypeAttributeGroup,PoolsAttributeGroup,HiddenAttributeGroup,LocaleAttributeGroup,SizeAttributeGroup,ValueAttributeGroup,PreviewimageAttributeGroup,FixvalueAttributeGroup,WidthAttributeGroup,HeightAttributeGroup,ShowtreeDeprecatedAttributeGroup,ShowtreeAttributeGroup,FilterqueryAttributeGroup,FilterattributeAttributeGroup,FilterAttributeGroup,FiltermodeAttributeGroup,FiltericAttributeGroup,FilterinvertAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=LinkSPTTag

" spt:number
syn region NumberSPTTag start=/<[\/]\?spt:number\>/ms=e-9 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,FixvalueAttributeGroup,LocaleAttributeGroup,DisabledAttributeGroup,ReadonlyAttributeGroup,SizeAttributeGroup,AlignAttributeGroup,AttributeGroup fold
sy cluster SPTTag add=NumberSPTTag

" spt:personalization
syn region PersonalizationSPTTag start=/<[\/]\?spt:personalization\>/ms=e-18 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,InformationAttributeGroup,PublisherAttributeGroup,ModeAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=PersonalizationSPTTag

" spt:prehtml TODO: [tagname]-[tag-attribute]
syn region PrehtmlSPTTag start=/<[\/]\?spt:prehtml\>/ms=e-10 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ObjectAttributeGroup fold
sy cluster SPTTag add=PrehtmlSPTTag

" spt:smarteditor
syn region SmarteditorSPTTag start=/<[\/]\?spt:smarteditor\>/ms=e-14 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,TextlabelAttributeGroup,RowsAttributeGroup,ColsAttributeGroup,ValueAttributeGroup,OptionsAttributeGroup,HideAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=SmarteditorSPTTag

" spt:spml
syn region SpmlSPTTag start=/<[\/]\?spt:spml\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=ApiAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=SpmlSPTTag

" spt:text
syn region TextSPTTag start=/<[\/]\?spt:text\>/ms=e-7 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,FixvalueAttributeGroup,LocaleAttributeGroup,TypeAttributeGroup,FormatAttributeGroup,DisabledAttributeGroup,ReadonlyAttributeGroup,SizeAttributeGroup,HyphenEditorAttributeGroup,InputTypeAttributeGroup,AttributeGroup fold
sy cluster SPTTag add=TextSPTTag

" spt:textarea
syn region TextareaSPTTag start=/<[\/]\?spt:textarea\>/ms=e-11 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,FixvalueAttributeGroup,LocaleAttributeGroup,TypeAttributeGroup,FormatAttributeGroup,DisabledAttributeGroup,ReadonlyAttributeGroup,SizeAttributeGroup,HyphenEditorAttributeGroup,InputTypeAttributeGroup,AttributeGroup fold
sy cluster SPTTag add=TextareaSPTTag

" spt:timestamp
syn region TimestampSPTTag start=/<[\/]\?spt:timestamp\>/ms=e-12 end=/[\/]\?>/me=s-1 contains=ConnectAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=TimestampSPTTag

" spt:tinymce
syn region TinymceSPTTag start=/<[\/]\?spt:tinymce\>/ms=e-10 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,ValueAttributeGroup,FixvalueAttributeGroup,TypeAttributeGroup,DisabledAttributeGroup,ReadonlyAttributeGroup,RowsAttributeGroup,ColsAttributeGroup,ThemeAttributeGroup,PoolsAttributeGroup,ConfigAttributeGroup,ConfigextensionAttributeGroup,ConfigValuesAttributeGroup,ToggleAttributeGroup,AttributeGroup fold
sy cluster SPTTag add=TinymceSPTTag

" spt:updown
syn region UpdownSPTTag start=/<[\/]\?spt:updown\>/ms=e-9 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,FromAttributeGroup,ToAttributeGroup,ValueAttributeGroup,LocaleAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=UpdownSPTTag

" spt:upload
syn region UploadSPTTag start=/<[\/]\?spt:upload\>/ms=e-9 end=/[\/]\?>/me=s-1 contains=NameAttributeGroup,LocaleAttributeGroup,PreviewimageAttributeGroup,AttributeGroup fold
sy cluster SPTTag add=UploadSPTTag

" spt:worklist
syn region WorklistSPTTag start=/<[\/]\?spt:worklist\>/ms=e-11 end=/[\/]\?>/me=s-1 contains=CommandAttributeGroup,InformationIDAttributeGroup,PoolIDAttributeGroup,WorklistIDAttributeGroupAttributeGroup,AttributeErrorGroup fold
sy cluster SPTTag add=WorklistSPTTag

syn region Comment start="<%--" end="--%>" fold
syn keyword JSP page taglib
syn region JSPComment start="<%@" end="%>" contains=JSP,AttributeGroup,String fold
syn region XMLComment start="<!--" end="-->" contains=@SPTag,@SPTTag fold
syn match Number /[-\+]\?\d\+\.\?d*/
syn match Boolean /true\|false/
syn region StringEvaluation start=/\${/ skip=/\v\\./ end=/}/ contains=StringEvaluation contained
syn region String start=/\v"/ skip=/\v\\./ end=/\v"/ contains=StringEvaluation,Attribute,Boolean,Number,@SPTag,@SPTTag
syn region String start=/\v'/ skip=/\v\\./ end=/\v'/ contains=StringEvaluation,Attribute,Boolean,Number,@SPTag,@SPTTag

" highlight links
highlight default link SPMLTag Label
highlight default link XMLTag Operator
highlight default link Attribute Type
highlight default link XMLComment Comment
highlight default link JSPComment Comment
highlight default link JSP Macro
highlight default link StringEvaluation Function

highlight default link ArgumentSPTag SPMLTag
highlight default link AttributeSPTag SPMLTag
highlight default link BarcodeSPTag SPMLTag
highlight default link BreakSPTag SPMLTag
highlight default link CalendarSheetSPTag SPMLTag
highlight default link CheckboxSPTag SPMLTag
highlight default link CodeSPTag SPMLTag
highlight default link CollectionSPTag SPMLTag
highlight default link ConditionSPTag SPMLTag
highlight default link DiffSPTag SPMLTag
highlight default link ElseSPTag SPMLTag
highlight default link ElseifSPTag SPMLTag
highlight default link ErrorSPTag SPMLTag
highlight default link ExpireSPTag SPMLTag
highlight default link FilterSPTag SPMLTag
highlight default link ForSPTag SPMLTag
highlight default link FormSPTag SPMLTag
highlight default link HiddenSPTag SPMLTag
highlight default link IfSPTag SPMLTag
highlight default link IncludeSPTag SPMLTag
highlight default link IoSPTag SPMLTag
highlight default link IteratorSPTag SPMLTag
highlight default link JsonSPTag SPMLTag
highlight default link LinktreeSPTag SPMLTag
highlight default link LivetreeSPTag SPMLTag
highlight default link LogSPTag SPMLTag
highlight default link LoginSPTag SPMLTag
highlight default link LoopSPTag SPMLTag
highlight default link MapSPTag SPMLTag
highlight default link OptionSPTag SPMLTag
highlight default link PrintSPTag SPMLTag
highlight default link RadioSPTag SPMLTag
highlight default link RangeSPTag SPMLTag
highlight default link ReturnSPTag SPMLTag
highlight default link ScaleimageSPTag SPMLTag
highlight default link ScopeSPTag SPMLTag
highlight default link SelectSPTag SPMLTag
highlight default link SetSPTag SPMLTag
highlight default link SortSPTag SPMLTag
highlight default link SassSPTag SPMLTag
highlight default link SubinformationSPTag SPMLTag
highlight default link TagbodySPTag SPMLTag
highlight default link TextSPTag SPMLTag
highlight default link TextareaSPTag SPMLTag
highlight default link TextimageSPTag SPMLTag
highlight default link UploadSPTag SPMLTag
highlight default link UrlSPTag SPMLTag
highlight default link WarningSPTag SPMLTag
highlight default link WorklistSPTag SPMLTag

highlight default link CounterSPTTag SPMLTag
highlight default link DateSPTTag SPMLTag
highlight default link DiffSPTTag SPMLTag
highlight default link Email2imgSPTTag SPMLTag
highlight default link EncryptemailSPTTag SPMLTag
highlight default link EscapeemailSPTTag SPMLTag
highlight default link FormsolutionsSPTTag SPMLTag
highlight default link Id2urlSPTTag SPMLTag
highlight default link IlinkSPTTag SPMLTag
highlight default link ImageeditorSPTTag SPMLTag
highlight default link ImpSPTTag SPMLTag
highlight default link IteratorSPTTag SPMLTag
highlight default link LinkSPTTag SPMLTag
highlight default link NumberSPTTag SPMLTag
highlight default link PersonalizationSPTTag SPMLTag
highlight default link PrehtmlSPTTag SPMLTag
highlight default link SmarteditorSPTTag SPMLTag
highlight default link SpmlSPTTag SPMLTag
highlight default link TextSPTTag SPMLTag
highlight default link TextareaSPTTag SPMLTag
highlight default link TimestampSPTTag SPMLTag
highlight default link TinymceSPTTag SPMLTag
highlight default link UpdownSPTTag SPMLTag
highlight default link UploadSPTTag SPMLTag
highlight default link WorklistSPTTag SPMLTag

highlight default link AbsoluteAttribute Attribute
highlight default link AbsoluteAttributeGroup AttributeGroup
highlight default link ActionAttribute Attribute
highlight default link ActionAttributeGroup AttributeGroup
highlight default link AlignAttribute Attribute
highlight default link AlignAttributeGroup AttributeGroup
highlight default link AltAttribute Attribute
highlight default link AltAttributeGroup AttributeGroup
highlight default link AnchorAttribute Attribute
highlight default link AnchorAttributeGroup AttributeGroup
highlight default link ApiAttribute Attribute
highlight default link ApiAttributeGroup AttributeGroup
highlight default link ArgAttribute Attribute
highlight default link ArgAttributeGroup AttributeGroup
highlight default link ArgumentsAttribute Attribute
highlight default link ArgumentsAttributeGroup AttributeGroup
highlight default link AttributeAttribute Attribute
highlight default link AttributeAttributeGroup AttributeGroup
highlight default link AttributeError Error
highlight default link AttributeErrorGroup AttributeGroup
highlight default link AttributesAttribute Attribute
highlight default link AttributesAttributeGroup AttributeGroup
highlight default link BackgroundAttribute Attribute
highlight default link BackgroundAttributeGroup AttributeGroup
highlight default link Bgcolor2Attribute Attribute
highlight default link Bgcolor2AttributeGroup AttributeGroup
highlight default link BgcolorAttribute Attribute
highlight default link BgcolorAttributeGroup AttributeGroup
highlight default link CaptcharequiredAttribute Attribute
highlight default link CaptcharequiredAttributeGroup AttributeGroup
highlight default link CheckedAttribute Attribute
highlight default link CheckedAttributeGroup AttributeGroup
highlight default link ClassnameAttribute Attribute
highlight default link ClassnameAttributeGroup AttributeGroup
highlight default link ClientAttribute Attribute
highlight default link ClientAttributeGroup AttributeGroup
highlight default link CodeAttribute Attribute
highlight default link CodeAttributeGroup AttributeGroup
highlight default link CollectionAttribute Attribute
highlight default link CollectionAttributeGroup AttributeGroup
highlight default link Color2Attribute Attribute
highlight default link Color2AttributeGroup AttributeGroup
highlight default link ColorAttribute Attribute
highlight default link ColorAttributeGroup AttributeGroup
highlight default link ColsAttribute Attribute
highlight default link ColsAttributeGroup AttributeGroup
highlight default link CommandAttribute Attribute
highlight default link CommandAttributeGroup AttributeGroup
highlight default link ConditionAttribute Attribute
highlight default link ConditionAttributeGroup AttributeGroup
highlight default link ConfigAttribute Attribute
highlight default link ConfigAttributeGroup AttributeGroup
highlight default link ConfigextensionAttribute Attribute
highlight default link ConfigextensionAttributeGroup AttributeGroup
highlight default link ConfigvaluesAttribute Attribute
highlight default link ConfigvaluesAttributeGroup AttributeGroup
highlight default link ConnectAttribute Attribute
highlight default link ConnectAttributeGroup AttributeGroup
highlight default link ContentTypeAttribute Attribute
highlight default link ContentTypeAttributeGroup AttributeGroup
highlight default link ContenttypeAttribute Attribute
highlight default link ContenttypeAttributeGroup AttributeGroup
highlight default link ContextAttribute Attribute
highlight default link ContextAttributeGroup AttributeGroup
highlight default link ConvertAttribute Attribute
highlight default link ConvertAttributeGroup AttributeGroup
highlight default link DateAttribute Attribute
highlight default link DateAttributeGroup AttributeGroup
highlight default link DateformatAttribute Attribute
highlight default link DateformatAttributeGroup AttributeGroup
highlight default link DecimalformatAttribute Attribute
highlight default link DecimalformatAttributeGroup AttributeGroup
highlight default link DecodingAttribute Attribute
highlight default link DecodingAttributeGroup AttributeGroup
highlight default link DecryptAttribute Attribute
highlight default link DecryptAttributeGroup AttributeGroup
highlight default link DefaultAttribute Attribute
highlight default link DefaultAttributeGroup AttributeGroup
highlight default link DeleteAttribute Attribute
highlight default link DeleteAttributeGroup AttributeGroup
highlight default link DisabledAttribute Attribute
highlight default link DisabledAttributeGroup AttributeGroup
highlight default link ElementAttribute Attribute
highlight default link ElementAttributeGroup AttributeGroup
highlight default link EncodingAttribute Attribute
highlight default link EncodingAttributeGroup AttributeGroup
highlight default link EncryptAttribute Attribute
highlight default link EncryptAttributeGroup AttributeGroup
highlight default link EnctypeAttribute Attribute
highlight default link EnctypeAttributeGroup AttributeGroup
highlight default link EqAttribute Attribute
highlight default link EqAttributeGroup AttributeGroup
highlight default link ExcerptAttribute Attribute
highlight default link ExcerptAttributeGroup AttributeGroup
highlight default link ExpressionAttribute Attribute
highlight default link ExpressionAttributeGroup AttributeGroup
highlight default link FilterAttribute Attribute
highlight default link FilterAttributeGroup AttributeGroup
highlight default link FilterattributeAttribute Attribute
highlight default link FilterattributeAttributeGroup AttributeGroup
highlight default link FiltericAttribute Attribute
highlight default link FiltericAttributeGroup AttributeGroup
highlight default link FilterinvertAttribute Attribute
highlight default link FilterinvertAttributeGroup AttributeGroup
highlight default link FiltermodeAttribute Attribute
highlight default link FiltermodeAttributeGroup AttributeGroup
highlight default link FilterqueryAttribute Attribute
highlight default link FilterqueryAttributeGroup AttributeGroup
highlight default link FixvalueAttribute Attribute
highlight default link FixvalueAttributeGroup AttributeGroup
highlight default link FocalpointAttribute Attribute
highlight default link FocalpointAttributeGroup AttributeGroup
highlight default link Font2Attribute Attribute
highlight default link Font2AttributeGroup AttributeGroup
highlight default link FontAttribute Attribute
highlight default link FontAttributeGroup AttributeGroup
highlight default link Font_sizeAttribute Attribute
highlight default link Font_sizeAttributeGroup AttributeGroup
highlight default link Font_weightAttribute Attribute
highlight default link Font_weightAttributeGroup AttributeGroup
highlight default link FontcolorAttribute Attribute
highlight default link FontcolorAttributeGroup AttributeGroup
highlight default link FontnameAttribute Attribute
highlight default link FontnameAttributeGroup AttributeGroup
highlight default link Fontsize2Attribute Attribute
highlight default link Fontsize2AttributeGroup AttributeGroup
highlight default link FontsizeAttribute Attribute
highlight default link FontsizeAttributeGroup AttributeGroup
highlight default link FontstyleAttribute Attribute
highlight default link FontstyleAttributeGroup AttributeGroup
highlight default link Fontweight2Attribute Attribute
highlight default link Fontweight2AttributeGroup AttributeGroup
highlight default link FontweightAttribute Attribute
highlight default link FontweightAttributeGroup AttributeGroup
highlight default link FormAttribute Attribute
highlight default link FormAttributeGroup AttributeGroup
highlight default link FormatAttribute Attribute
highlight default link FormatAttributeGroup AttributeGroup
highlight default link FromAttribute Attribute
highlight default link FromAttributeGroup AttributeGroup
highlight default link GravityAttribute Attribute
highlight default link GravityAttributeGroup AttributeGroup
highlight default link GtAttribute Attribute
highlight default link GtAttributeGroup AttributeGroup
highlight default link GteAttribute Attribute
highlight default link GteAttributeGroup AttributeGroup
highlight default link GuiAttribute Attribute
highlight default link GuiAttributeGroup AttributeGroup
highlight default link HandlerAttribute Attribute
highlight default link HandlerAttributeGroup AttributeGroup
highlight default link HeightAttribute Attribute
highlight default link HeightAttributeGroup AttributeGroup
highlight default link HiddenAttribute Attribute
highlight default link HiddenAttribute Attribute
highlight default link HiddenAttributeGroup AttributeGroup
highlight default link HideAttribute Attribute
highlight default link HideAttributeGroup AttributeGroup
highlight default link HyphenEditorAttribute Attribute
highlight default link HyphenEditorAttributeGroup AttributeGroup
highlight default link IcAttribute Attribute
highlight default link IcAttributeGroup AttributeGroup
highlight default link IdAttribute Attribute
highlight default link IdAttributeGroup AttributeGroup
highlight default link ImageAttribute Attribute
highlight default link ImageAttributeGroup AttributeGroup
highlight default link IndentAttribute Attribute
highlight default link IndentAttributeGroup AttributeGroup
highlight default link IndexAttribute Attribute
highlight default link IndexAttributeGroup AttributeGroup
highlight default link InformationAttribute Attribute
highlight default link InformationAttributeGroup AttributeGroup
highlight default link InformationIDAttribute Attribute
highlight default link InformationIDAttributeGroup AttributeGroup
highlight default link InsertAttribute Attribute
highlight default link InsertAttributeGroup AttributeGroup
highlight default link InvertAttribute Attribute
highlight default link InvertAttributeGroup AttributeGroup
highlight default link IsNullAttribute Attribute
highlight default link IsNullAttributeGroup AttributeGroup
highlight default link ItemAttribute Attribute
highlight default link ItemAttributeGroup AttributeGroup
highlight default link ItemtextAttribute Attribute
highlight default link ItemtextAttributeGroup AttributeGroup
highlight default link KeyAttribute Attribute
highlight default link KeyAttributeGroup AttributeGroup
highlight default link KeysAttribute Attribute
highlight default link KeysAttributeGroup AttributeGroup
highlight default link LanguageAttribute Attribute
highlight default link LanguageAttributeGroup AttributeGroup
highlight default link LeaflinkAttribute Attribute
highlight default link LeaflinkAttributeGroup AttributeGroup
highlight default link LevelAttribute Attribute
highlight default link LevelAttributeGroup AttributeGroup
highlight default link LinkcolorAttribute Attribute
highlight default link LinkcolorAttributeGroup AttributeGroup
highlight default link ListAttribute Attribute
highlight default link ListAttributeGroup AttributeGroup
highlight default link LocaleAttribute Attribute
highlight default link LocaleAttributeGroup AttributeGroup
highlight default link LocalelinkAttribute Attribute
highlight default link LocalelinkAttributeGroup AttributeGroup
highlight default link LoginAttribute Attribute
highlight default link LoginAttributeGroup AttributeGroup
highlight default link LookupAttribute Attribute
highlight default link LookupAttributeGroup AttributeGroup
highlight default link LtAttribute Attribute
highlight default link LtAttributeGroup AttributeGroup
highlight default link LteAttribute Attribute
highlight default link LteAttributeGroup AttributeGroup
highlight default link ManipulateAttribute Attribute
highlight default link ManipulateAttributeGroup AttributeGroup
highlight default link MatchAttribute Attribute
highlight default link MatchAttributeGroup AttributeGroup
highlight default link MaxAttribute Attribute
highlight default link MaxAttributeGroup AttributeGroup
highlight default link MethodAttribute Attribute
highlight default link MethodAttributeGroup AttributeGroup
highlight default link MinAttribute Attribute
highlight default link MinAttributeGroup AttributeGroup
highlight default link ModeAttribute Attribute
highlight default link ModeAttributeGroup AttributeGroup
highlight default link ModuleAttribute Attribute
highlight default link ModuleAttributeGroup AttributeGroup
highlight default link MultipleAttribute Attribute
highlight default link MultipleAttributeGroup AttributeGroup
highlight default link NameAttribute Attribute
highlight default link NameAttributeGroup AttributeGroup
highlight default link NameencodingAttribute Attribute
highlight default link NameencodingAttributeGroup AttributeGroup
highlight default link NeqAttribute Attribute
highlight default link NeqAttributeGroup AttributeGroup
highlight default link NodeAttribute Attribute
highlight default link NodeAttributeGroup AttributeGroup
highlight default link NowButtonAttribute Attribute
highlight default link NowButtonAttributeGroup AttributeGroup
highlight default link ObjectAttribute Attribute
highlight default link ObjectAttributeGroup AttributeGroup
highlight default link ObjektAttribute Attribute
highlight default link ObjektAttributeGroup AttributeGroup
highlight default link OffsetAttribute Attribute
highlight default link OffsetAttributeGroup AttributeGroup
highlight default link OnclickAttribute Attribute
highlight default link OnclickAttributeGroup AttributeGroup
highlight default link OptionsAttribute Attribute
highlight default link OptionsAttributeGroup AttributeGroup
highlight default link OverwriteAttribute Attribute
highlight default link OverwriteAttributeGroup AttributeGroup
highlight default link PaddingAttribute Attribute
highlight default link PaddingAttributeGroup AttributeGroup
highlight default link PaddingcolorAttribute Attribute
highlight default link PaddingcolorAttributeGroup AttributeGroup
highlight default link ParentlinkAttribute Attribute
highlight default link ParentlinkAttributeGroup AttributeGroup
highlight default link PasswordAttribute Attribute
highlight default link PasswordAttributeGroup AttributeGroup
highlight default link PoolIdAttribute Attribute
highlight default link PoolIdAttributeGroup AttributeGroup
highlight default link PoolsAttribute Attribute
highlight default link PoolsAttributeGroup AttributeGroup
highlight default link PopupheightAttribute Attribute
highlight default link PopupheightAttributeGroup AttributeGroup
highlight default link PopupwidthAttribute Attribute
highlight default link PopupwidthAttributeGroup AttributeGroup
highlight default link PreviewimageAttributeGroup AttributeGroup
highlight default link PublisherAttribute Attribute
highlight default link PublisherAttributeGroup AttributeGroup
highlight default link QualityAttribute Attribute
highlight default link QualityAttributeGroup AttributeGroup
highlight default link QueryAttribute Attribute
highlight default link QueryAttributeGroup AttributeGroup
highlight default link QuerystringAttribute Attribute
highlight default link QuerystringAttributeGroup AttributeGroup
highlight default link RangeAttribute Attribute
highlight default link RangeAttributeGroup AttributeGroup
highlight default link ReadonlyAttribute Attribute
highlight default link ReadonlyAttributeGroup AttributeGroup
highlight default link ReturnAttribute Attribute
highlight default link ReturnAttributeGroup AttributeGroup
highlight default link RoleAttribute Attribute
highlight default link RoleAttributeGroup AttributeGroup
highlight default link RootElementAttribute Attribute
highlight default link RootElementAttributeGroup AttributeGroup
highlight default link RootelementAttribute Attribute
highlight default link RootelementAttributeGroup AttributeGroup
highlight default link RowsAttribute Attribute
highlight default link RowsAttributeGroup AttributeGroup
highlight default link ScalestepsAttribute Attribute
highlight default link ScalestepsAttributeGroup AttributeGroup
highlight default link ScopeAttribute Attribute
highlight default link ScopeAttributeGroup AttributeGroup
highlight default link SelectedAttribute Attribute
highlight default link SelectedAttributeGroup AttributeGroup
highlight default link SeparatorAttribute Attribute
highlight default link SeparatorAttributeGroup AttributeGroup
highlight default link SequencesAttribute Attribute
highlight default link SequencesAttributeGroup AttributeGroup
highlight default link SessionAttribute Attribute
highlight default link SessionAttributeGroup AttributeGroup
highlight default link ShowtreeAttribute Attribute
highlight default link ShowtreeDeprecatedAttribute SpellBad
highlight default link ShowtreeAttributeGroup AttributeGroup
highlight default link SizeAttribute Attribute
highlight default link SizeAttributeGroup AttributeGroup
highlight default link SortkeysAttribute Attribute
highlight default link SortkeysAttributeGroup AttributeGroup
highlight default link SortsequencesAttribute Attribute
highlight default link SortsequencesAttributeGroup AttributeGroup
highlight default link SorttypesAttribute Attribute
highlight default link SorttypesAttributeGroup AttributeGroup
highlight default link SourceAttribute Attribute
highlight default link SourceAttributeGroup AttributeGroup
highlight default link StepAttribute Attribute
highlight default link StepAttributeGroup AttributeGroup
highlight default link StyleAttribute Attribute
highlight default link StyleAttributeGroup AttributeGroup
highlight default link TemplateAttribute Attribute
highlight default link TemplateAttributeGroup AttributeGroup
highlight default link TextAttribute Attribute
highlight default link TextAttributeGroup AttributeGroup
highlight default link Text_transformAttribute Attribute
highlight default link Text_transformAttribute Attribute
highlight default link Text_transformAttributeGroup AttributeGroup
highlight default link Text_transformAttributeGroup AttributeGroup
highlight default link TextlabelAttribute Attribute
highlight default link TextlabelAttributeGroup AttributeGroup
highlight default link ThemeAttribute Attribute
highlight default link ThemeAttributeGroup AttributeGroup
highlight default link TitleAttribute Attribute
highlight default link TitleAttributeGroup AttributeGroup
highlight default link ToAttribute Attribute
highlight default link ToAttributeGroup AttributeGroup
highlight default link ToggleAttribute Attribute
highlight default link ToggleAttributeGroup AttributeGroup
highlight default link TransformAttribute Attribute
highlight default link TransformAttributeGroup AttributeGroup
highlight default link TypeAttribute Attribute
highlight default link TypeAttributeGroup AttributeGroup
highlight default link TypesAttribute Attribute
highlight default link TypesAttributeGroup AttributeGroup
highlight default link UriAttribute Attribute
highlight default link UriAttributeGroup AttributeGroup
highlight default link UrlAttribute Attribute
highlight default link UrlAttributeGroup AttributeGroup
highlight default link UrlonlyAttribute Attribute
highlight default link UrlonlyAttributeGroup AttributeGroup
highlight default link UrlparamAttribute Attribute
highlight default link UrlparamAttributeGroup AttributeGroup
highlight default link UserAttribute Attribute
highlight default link UserAttributeGroup AttributeGroup
highlight default link ValueAttribute Attribute
highlight default link ValueAttributeGroup AttributeGroup
highlight default link VarNameAttribute Attribute
highlight default link VarNameAttributeGroup AttributeGroup
highlight default link VarnameAttribute Attribute
highlight default link VarnameAttributeGroup AttributeGroup
highlight default link WidthAttribute Attribute
highlight default link WidthAttributeGroup AttributeGroup
highlight default link WindowAttribute Attribute
highlight default link WindowAttributeGroup AttributeGroup
highlight default link WorklistIDAttribute Attribute
highlight default link WorklistIDAttributeGroup AttributeGroup

let b:current_syntax = "spml"

