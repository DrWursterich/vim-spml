# Vim-SPML

This repository contains a Vim plugin for SPML.

## Installation

### Pathogen

If Pathogen is installed just execute the following:
```bash
cd ~/.vim/bundle
git clone git@gitlab.com:DrWursterich/vim-spml.git
```

### Vundle

If you are using Vundle insert the following into your `.vimrc`:
```vim
Plugin 'https://gitlab.com/DrWursterich/vim-spml.git'
```

Install the new Plugin from inside vim:
```vim
:PluginInstall
```

Or from the command line:
```bash
vim +PluginInstall +qall
```

See [Vundle](https://github.com/VundleVim/Vundle.vim) for details.

### Manual

A manual installation under a Unix-based system should look like this:
 ```bash
git clone git@gitlab.com:DrWursterich/vim-spml.git vim-spml
find vim-spml -mindepth 1 -maxdepth 1 -type d -print0 | xargs -0 -I {} cp -r "{}" ~/.vim
rm -rf vim-spml
 ```
